<?php


function crum_features() {
    $labels = array(
        'name'               => __( 'Features', 'crum' ),
        'singular_name'      => __( 'Feature', 'crum' ),
        'add_new_item'       => __( 'Add New Feature', 'crum' ),
        'edit_item'          => __( 'Edit Feature', 'crum' ),
        'new_item'           => __( 'New Feature', 'crum' ),
        'all_items'          => __( 'All Features', 'crum' ),
        'view_item'          => __( 'View Feature', 'crum' ),
        'search_items'       => __( 'Search Feature', 'crum' ),
        'not_found'          => __( 'No Feature found', 'crum' ),
        'not_found_in_trash' => __( 'No Feature found in the Trash', 'crum' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Features blocks'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Features blocks',
        'public'        => true,
        'supports'      => array( 'title', 'editor'),
        'menu_icon'     => get_template_directory_uri() . '/assets/images/features-icon.png', /* the icon for the custom post type menu */
        'has_archive'   => false
    );
    register_post_type( 'features', $args );
}
add_action( 'init', 'crum_features' );




    function crumina_mega_menu() {
        $labels = array(
            'name' => __('Mega menu', 'crum'),
            'singular_name' => __('Crumina Mega Menu', 'crum'),
            'add_new_item' => __('Add New Menu Item', 'crum'),
            'edit_item' => __('Edit Menu Item', 'crum'),
            'search_items' => __('Search Menu Items', 'crum'),
            'not_found' => __('Sorry: Menu Item Not Found', 'crum'),
            'not_found_in_trash' => __('Sorry: Menu Item Not Found In Trash', 'crum'),

        );
        $args = array(
            'labels'        => $labels,
            'rewrite' => false,
            'public' => true,
            'hierarchical' => 'false',
            'capability_type' => 'page',
            'supports' => array('title', 'editor'),
            'menu_icon' => get_template_directory_uri() . '/assets/images/menu-icon.png', /* the icon for the custom post type menu */
            'has_archive'   => false
        );
        register_post_type( 'crumina_mega_menu', $args );
    }
    add_action( 'init', 'crumina_mega_menu' );
