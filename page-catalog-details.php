<?php
/*
Template Name: Catalog Service Detail Page
 */
get_template_part('templates/top','page');
 ?>

<section id="layout">
    <div class="row">

        <div class="blog-section sidebar-left">
            <section id="main-content" role="main" class="nine columns">

	            <h1>Service Details (Service Catalog 2013-2014)</h1>
				
				<p>Please look over the information below to see if this service matches your needs. If it does, you may contact the Service Desk for assistance in putting in your service request. Please use the 9-digit Service Number (44.00.02.001) and the Service Unit name to refer to the service.</p>
				
				<div class="serviceDetails">
					<h4>Service Name:</h4>
					<p>PeopleSoft - CRM Mass Communication Tool - 44.00.02.001</p>
					
					<h4>Price:</h4>
					<p>$ 0.00 Per Service Monthly</p>
					
					<h4>Brief Description:</h4>
					<p>Customer relationship management software</p>
					
					<h4>Detailed Description:</h4>
					<p>Primarily a communication tool, the customer relationship management (CRM) software serves as a platform to communicate critical information involving student records and/or employment opportunities at a mass level.</p>
					
					<h4>Included Functionality:</h4>
					<p>N/A</p>
					
					<h4>Technical Specifications:</h4>
					<p>N/A</p>
					
					<h4>Service Components:</h4>
					<p>N/A</p>
					
					<h4>Optional Components:</h4>
					<p>N/A</p>
					
					<h4>Authorized Customers:</h4>
					<p>All UCF departments or organizations.  Note: Department data managers of the specific ViewStar library must approve user request before CS&T can grant user data access.</p>
					
					<h4>Available Service Locations:</h4>
					<p>All UCF Campuses and Research Park</p>
					
					<h4>Audience:</h4>
					<p>Faculty, Staff</p>
					
					<h4>Dependencies:</h4>
					<p>N/A</p>
					
					<h4>Required Additional Services:</h4>
					<p>none</p>
					
					<h4>Service Unit:</h4>
					<p>CS&amp;T Enterprise Application Development</p>
					
					<h4>Service Owner:</h4>
					<p>Applications System Analyst/Programmer</p>
					
					<h4>Service Number:</h4>
					<p>44.00.02.001</p>
					
					<h4>Signed SLA Required:</h4>
					<p>Yes</p>
				</div>

            </section>
            <?php get_template_part('templates/sidebar', 'left'); ?>
        </div>

    </div>
</section>