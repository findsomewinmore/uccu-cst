jQuery(document).ready( function($) {
		//Gets a list of all current pages in the sidebar
    	var currentPages = $('.widget_wenderhost-subpages .current_page_item, .widget_wenderhost-subpages .current_page_parent, .widget_wenderhost-subpages .current_page_ancestor, #layout .widget_nav_menu .current_page_item, #layout .widget_nav_menu .current_page_parent, #layout .widget_nav_menu .current_page_ancestor');
    	
    	//Adds classes to list items that have children
		$('.widget_wenderhost-subpages li, #layout .widget_nav_menu li').has('ul.children, ul.sub-menu').addClass('slide_toggle').prepend('<div class="sidebar_toggle_button"></div>');
		
		//Adds active class to current pages
		currentPages.has('ul.children, ul.sub-menu').addClass('sidebar_active').find('> ul').css('display', 'block');
		
		$('.widget_wenderhost-subpages li div.sidebar_toggle_button, #layout .widget_nav_menu li div.sidebar_toggle_button').click(function() {			
			$(this).parent().toggleClass('sidebar_active').find('>ul').slideToggle();
		});
		
});