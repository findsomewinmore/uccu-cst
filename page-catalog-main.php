<?php
/*
Template Name: Catalog Main
 */
get_template_part('templates/top','page');
 ?>

<section id="layout">
  <div class="row">
    <div class="blog-section sidebar-left">
      <section id="main-content" role="main" class="nine columns">
        <?php while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; ?>

        <div class="catalogListing">
          <?php
          // lets start the magic
          mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Unable to connect to MySQL.");
          @mysql_select_db(DB_NAME) or die("Unable to select database.");

          $mainsql    = "select distinct maincat from servicecats order by maincat";
          $mainresult = mysql_query($mainsql);
          ?>

          <?php while ($mainrow = mysql_fetch_row($mainresult)): ?>
            <?php $maincatname = $mainrow[0]; ?>
            <?php $maincatclass = strtolower(str_replace(" ","-", str_replace("&","",$mainrow[0]))); ?>

            <div class="catalogCategory">
              <h2 class="<?= $maincatclass ?> wpb_toggle"><?= $maincatname ?></h2>
                <div class="wpb_toggle_content">
                    <ul>
                      <?php $subsql    = "select distinct subcat from servicecats where maincat = '$maincatname' order by subcat"; ?>
                      <?php $subresult = mysql_query($subsql); ?>

                      <?php while ($subrow = mysql_fetch_row($subresult)): ?>
                        <li>
                          <h4 class="wpb_toggle"><?= $subrow[0] ?></h4>

                          <div class="wpb_toggle_content">
                            <ul class="subServices">
                              <?php $results = $wpdb->get_results( "select post_id, meta_key from $wpdb->postmeta where meta_value = '".$subrow[0]."'", ARRAY_A ); ?>

                              <?php foreach ($results as $post): ?>
                                <?php $id = $post['post_id']; ?>

                                <li><a href="<?= get_permalink($id)?>"><?= get_the_title($id);  ?></a></li>
                              <?php endforeach ?>
                          </ul>
                      </div>

                      <div class="last_toggle_el_margin"></div>
                    </li>
                  <?php endwhile; ?>
                </ul>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </section>

      <?php get_template_part('templates/sidebar', 'left'); ?>
    </div>
  </div>
</section>
