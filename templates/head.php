<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

    <?php $options = get_option('maestro'); ?>

    <meta charset="utf-8">

    <title>
        <?php if (is_home() || is_front_page()) {
        bloginfo('name');
    } else {
        wp_title('');
    }?>
    </title>
<?php  if(isset($options["custom_favicon"])){ ?>
    <link rel="icon" type="image/png" href="<?php echo $options["custom_favicon"] ?>">
<?php } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--[if lte IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <?php  if(is_page()){
        global $post;
        $p_bg_color = get_post_meta( $post->ID, 'crum_page_custom_bg_color', true );
        $p_bg_image = get_post_meta( $post->ID, 'crum_page_custom_bg_image', true );
        $p_bg_fixed = get_post_meta( $post->ID, 'crum_page_custom_bg_fixed', true );
        $p_bg_repeat = get_post_meta( $post->ID, 'crum_page_custom_bg_repeat', true );
        ?>
        <style type="text/css">
            <?php if ($p_bg_image || ($p_bg_color && (($p_bg_color) !='#ffffff'))){ ?>
            body{
                background-color: <?php echo $p_bg_color; if ($p_bg_image){ echo ' url('.$p_bg_image.') center 0 '.$p_bg_repeat.';';} ?>
            <?php if ($p_bg_fixed) echo 'background-attachment: fixed;' ?>
            }
            #change_wrap_div.white-skin {background:#fff; max-width: 1220px; margin: 0 auto; box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.2);
            <?php } ?>
        </style>
    <?php } ?>
	<!-- ADD UCF HEADER -->
		<script type="text/javascript" src="http://universityheader.ucf.edu/bar/js/university-header.js"></script>
	<!-- END UCF HEADER -->
	
	
    <?php wp_enqueue_script( 'wpb_composer_front_js');
    		wp_enqueue_script('site-scripts'); 
    	  wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
</head>

