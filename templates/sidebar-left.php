<?php 
if(is_single()){
        global $post;
        $page_id = $post->ID;
        
    } else {
        $page_id     = get_queried_object_id();
    }
    $parents = get_post_ancestors($page_id);
    if($page_id != 20 || $page_id != 3936 || $page_id != 3938 || $page_id != 3940 || $page_id != 3942 || $page_id != 3944 || $page_id != 3946) { 
	    foreach($parents as $page_id){
			if($page_id == 20){
		
				$serviceCatalogClass = "serviceCatSidebar";  
		
				break; //Match found, no need to keep checking
			}
		}
    }
?>
<aside class="three columns <?php echo $serviceCatalogClass ?>" id="left-sidebar">

    <?php

    $selected_sidebar = get_post_meta($page_id, 'crum_sidebars_sidebar_1', true);

    if ($selected_sidebar) {

        crum_custom_dynamic_sidebar($selected_sidebar);

    } else {
    
    	if($serviceCatalogClass != "") { 
    		 dynamic_sidebar('service_catalog_sidebar');
		} else {
			dynamic_sidebar('sidebar-left');
		}
        

    }
    ?>

</aside>