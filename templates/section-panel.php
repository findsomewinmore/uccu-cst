<?php $options = get_option('maestro'); ?>

<div id="top-panel" class="open">
    <div class="row">
        <div class="top-panel-inner">
            
			<div class="five columns top-text">
                <i class="icon <?php
                    if ($options["top_panel_icon"]) {
                        echo $options["top_panel_icon"];
                    } else {
                        echo 'awesome-list';
                    } ?>">
                </i>
                <div class="title"><?php echo $options["top_panel_title"]; ?></div>
                <?php echo $options["top_panel_text"]; ?>

                
            </div>
            <div class="five offset-by-one columns">
            	<?php if(function_exists('tptn_show_pop_posts')) tptn_show_pop_posts('limit=3'); ?>
            	<?php if ($options["top_panel_login"] !='off') { ?>

            <div class="three columns">
                <div class="top-login">
                    <?php

                    if (!is_user_logged_in()) {

                        wp_login_form();

                    } else {
                        global $user_login;
                        get_currentuserinfo();
                        $current_user = wp_get_current_user(); ?>

                        <h3><?php _e('Welcome', 'crum'); echo ', ' . $user_login . '!'; ?></h3>
                        <div class="top-avatar">
                            <?php if (($current_user instanceof WP_User)) {
                                echo get_avatar($current_user->user_email, 80);
                            }
                            ?>
                        </div>

                        <div class="links">
                            <?php wp_loginout(); ?> &nbsp;&nbsp; <?php  wp_register('', ''); ?>
                        </div>


                    <?php }/* } else { ?>

                        <h3><?php _e('login on site', 'crum'); ?></h3>
                        <form name="loginform" id="loginform" action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">

                            <input type="text" name="log" id="user_login" class="input" value="" size="20" tabindex="10" placeholder="<?php _e('Enter your login', 'crum'); ?>"/>
                            <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" tabindex="20" placeholder="<?php _e('Enter your password', 'crum'); ?>"/>

                            <div class="links">
                                <?php if (get_option('users_can_register')) { ?>
                                    <a class="reg" href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=register" tabindex="100"><?php _e('Registration', 'crum'); ?></a>
                                <?php } ?>
                                <a class="submit" href="#" name="wp-submit"><?php _e('Press ENTER to login', 'crum'); ?></a>
                                <input type="submit" style="width:0; height: 0; overflow: hidden; padding: 0; border: none" hidden="hidden">
                            </div>
                        </form>

                        <?php } */?>
                </div>
            </div>
                <?php } ?>
            </div>
        </div>
        <a id="open-top-panel" href="#"></a>
    </div>
</div>