<section id="footer">
    <div class="row">    	
    	
        <div class="four columns">
        	<div class="widget">
		        	<h3 class="widget-title">Service Catalog</h3>
		            <div class="catalogListing">
			            <ul>
							<?php wp_list_pages('title_li=&child_of=20&depth=1'); ?>
			            </ul>
					</div>
			</div>
			<?php dynamic_sidebar('sidebar-footer-col1');?>
        </div>
        <div class="four columns">
             <?php dynamic_sidebar('sidebar-footer-col2');?>
        </div>
        <div class="four columns">
			<?php dynamic_sidebar('sidebar-footer-col3');?>
		</div>
        
        
    </div>
</section>