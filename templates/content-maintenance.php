<?php $options = get_option('maestro'); ?>
<?php query_posts($query_string . 'category_name=upcoming-maintenance&orderby=date&order=ASC'); ?>
<?php  if (!have_posts()) : ?>

    <article id="post-0" class="post no-results not-found">
        <header class="entry-header">
            <h1><?php _e( 'Nothing Found', 'crum' ); ?></h1>
        </header>

        <div class="entry-content">
            <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'crum' ); ?></p>
            <?php get_search_form(); ?>
        </div>


        <header class="entry-header">
            <h2><?php _e('Tags also can be used', 'crum'); ?></h2>
        </header>

        <div class="tags-widget">
            <?php wp_tag_cloud('smallest=10&largest=10&number=30'); ?>
        </div>

    </article><!-- #post-0 -->
    <?php endif; ?>

<?php while (have_posts()) : the_post();

     $options = get_option('maestro');
$image_crop = $options['thumb_image_crop'];
if ($image_crop == "") {$image_crop = true;}
?>
<article <?php post_class(); ?>>

    <div class="post-media">
        <?php

        if (has_post_format('video')) {
            get_template_part('templates/post', 'video');
        } elseif (has_post_format('audio')) {
            get_template_part('templates/post', 'audio');
        } elseif (has_post_format('gallery')) {
            get_template_part('templates/post', 'gallery');
        } else {

            if (has_post_thumbnail()) {
                $thumb = get_post_thumbnail_id();
                $img_url = wp_get_attachment_url($thumb, 'full'); //get img URL
                if ($options['post_thumbnails_width'] != '' && $options['post_thumbnails_height'] != '') {
                    $article_image = aq_resize($img_url, $options['post_thumbnails_width'], $options['post_thumbnails_height'], $image_crop);
                } else {
                    $article_image = aq_resize($img_url, 1200, 500, $image_crop);
                }

                ?>

                <div class="entry-thumb">
                    <img src="<?php echo $article_image ?>" style="margin:0 0;" alt="<?php the_title();?>" title="<?php the_title();?>">
                    <span class="hover-box">
                        <a href="<?php the_permalink(); ?>" class="more-link"> </a>
                        <a href="<?php echo $img_url; ?>" class="zoom-link"> </a>
                    </span>
                </div>

            <?php
            }
        } ?>

    </div>


    <div class="clearfif cl">
        <header>
        	<time class="updated" datetime="<?php echo get_the_time('c'); ?>">
                <span class="day"><?php echo get_the_date('d'); ?></span>
                <span class="month"><?php echo get_the_date('M'); ?></span>
            </time>

            <div class="header-wrap ovh">
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
               <div class="entry-meta"><strong>Scheduled for <?php echo get_the_time('m/d/Y h:i A'); ?></strong></div>
            </div>
        </header>
        <div class="entry-content">
            <?php the_content();  ?>
            
            <p>
			<em>Scheduled Start: <?php echo get_the_time('m/d/Y h:i A'); ?></em><br />
            <em>Scheduled End: <?php echo do_shortcode('[postexpirator]'); ?></em><br />
            <em>Contact: <?php the_field('maintenance_contact'); ?></em>
            </p>
        </div>
    </div>

</article>

<?php endwhile; ?>

<?php if ($wp_query->max_num_pages > 1) : ?>

<nav class="page-nav">

    <?php next_posts_link(''); ?>

    <?php previous_posts_link(''); ?>

</nav>

<?php endif; ?>
