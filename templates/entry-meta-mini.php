<div class="entry-meta post-info">
    <span class="entry-date"><?php echo get_the_date(); ?></span>

    <span class="post-tags">, <?php the_category(','); ?></span>

</div>