
    <?php

    if(is_single()){
        global $post;
        $page_id = $post->ID;
    } else {
        $page_id     = get_queried_object_id();
    }

    $selected_sidebar = get_post_meta($page_id, 'crum_sidebars_sidebar_3', true);

    if ($selected_sidebar) {

        crum_custom_dynamic_sidebar($selected_sidebar);

    } else {

        dynamic_sidebar('sidebar-top');

    }
    ?>