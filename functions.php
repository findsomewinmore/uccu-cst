<?php
/**
 * Crumina themes functions
 */

function is_child($page_id)
{
    global $post;

    if (is_page() && ($post->post_parent == $page_id) || is_page($pid) ) {
        return true;
    } else {
        return false;
    }
}

//wp_register_script( 'columnizer', get_stylesheet_directory_uri(). '/assets/js/jquery.columnizer.js', 'jquery', true );
wp_register_script( 'site-scripts', get_stylesheet_directory_uri(). '/assets/js/site.js', 'jquery', true );

unregister_sidebar('sidebar-footer-col3');

register_sidebar(array(
    'name'          => __('Service Catalog', 'crum'),
    'id'            => 'service_catalog_sidebar',
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
));

register_sidebar(array(
  'name'          => __('About', 'crum'),
  'id'            => 'about_section_sidebar',
  'before_widget' => '<section id="%1$s" class="widget %2$s">',
  'after_widget'  => '</section>',
  'before_title'  => '<h3 class="widget-title">',
  'after_title'   => '</h3>',
));

add_filter('tribe_events_event_schedule_details_formatting', 'tribe_event_listing_details_filter', 10, 2);

function tribe_event_listing_details_filter($formatting_details)
{
    $formatting_details['time'] = false;

    return $formatting_details;
}
