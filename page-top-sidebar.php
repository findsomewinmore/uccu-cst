<?php
/*
Template Name: Page with top sidebar
 */
 ?>
 
<section id="fullWidth">
    <div class="row">
        <div class="twelve columns">
			<?php get_template_part('templates/sidebar', 'top'); ?>
        </div>
    </div>
</section>

<section id="layout" class="no-title">
    <div class="row">
        <div class="twelve columns">

        <?php get_template_part('templates/content', 'page'); ?>

        </div>
    </div>
</section>